export interface Products {
  count: number;
  products: Product[];
}

export interface Product {
  id: Number;
  name: String;
  image: String;
  price: Number;
  quantity: Number;
}
