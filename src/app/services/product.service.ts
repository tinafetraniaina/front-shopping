import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Products, Product } from '../shared/model/product';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  params: any;
  protected headers = new HttpHeaders();
  model: any;
  private url = "https://dev.techtablet.fr/apiv2/";

  constructor(private http: HttpClient) {
    this.params = { 'key': 'id' };
    this.headers.set('Accept', 'application/json');
    this.headers.set('Origin', '*');
  }

  getAllProducts(): Observable<Products> {
    const params = this.params;
    const headers = this.headers;
    return this.http.get<any>(this.url + 'products', { params, headers }).pipe(
      map((res) => {
        return res.slice(-10).forEach();
      })
    );
  }

  getSingleProduct(id: Number): Observable<any> {
    return this.http.get(this.url + 'products/' + id).pipe(
      map((res) => {
        return res;
      })
    );
  }
}
