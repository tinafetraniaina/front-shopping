import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { CartComponent } from './components/cart/cart.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  // {
  //   path: 'profile',
  //   component: ProfileComponent,
  //   canActivate: [AuthGuardService],
  // },
  // { path: 'product/:id', component: ProductComponent },
  { path: 'cart', component: CartComponent },
  // {
  //   path: 'checkout',
  //   component: CheckoutComponent,
  //   canActivate: [AuthGuardService],
  // },
  // {
  //   path: 'order-history',
  //   component: OrderHistoryComponent,
  //   canActivate: [AuthGuardService],
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
