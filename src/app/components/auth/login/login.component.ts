import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  showpassword: boolean = false;
  error = '';
  loading = false;

  inputForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern(
        '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'
      ),
    ]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
  });

  constructor(private _auth: AuthService, private _router: Router, private toastr: ToastService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.loading = true;
    this.error = '';
    const inputUser = this.inputForm.getRawValue();
    if (inputUser.email && inputUser.password) {
      this._auth.login({ email: inputUser.email, password: inputUser.password }).subscribe(
        (res) => {
          this.loading = false;
          this._router.navigate(['/']);
        },
        (err) => {
          this.error = err.error.message;
          this.loading = false;
          this.toastr.error(this.error);
        }
      );
    }
  }

  show() {
    this.showpassword = !this.showpassword;
  }

}
